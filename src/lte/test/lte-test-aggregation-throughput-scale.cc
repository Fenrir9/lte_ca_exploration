/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2017 Alexander Krotov
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Alexander Krotov <krotov@iitp.ru>
 *
 */

#include "lte-test-aggregation-throughput-scale.h"

#include <algorithm>
#include <numeric>

#include <ns3/application-container.h>
#include <ns3/friis-spectrum-propagation-loss.h>
#include <ns3/internet-stack-helper.h>
#include <ns3/ipv4-address-helper.h>
#include <ns3/ipv4-interface-container.h>
#include <ns3/ipv4-static-routing-helper.h>
#include <ns3/log.h>
#include <ns3/lte-enb-net-device.h>
#include <ns3/lte-helper.h>
#include <ns3/lte-ue-net-device.h>
#include <ns3/lte-ue-rrc.h>
#include <ns3/mobility-helper.h>
#include <ns3/net-device-container.h>
#include <ns3/node-container.h>
#include <ns3/point-to-point-epc-helper.h>
#include <ns3/point-to-point-helper.h>
#include <ns3/simulator.h>
#include <ns3/udp-client.h>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("LteAggregationThroughputScaleTest");

LteAggregationThroughputScaleTestSuite::LteAggregationThroughputScaleTestSuite ()
  : TestSuite ("lte-aggregation-throughput-scale", SYSTEM)
{
  AddTestCase (new LteAggregationThroughputScaleTestCase ("Carrier aggregation throughput scale"), TestCase::QUICK);
}

static LteAggregationThroughputScaleTestSuite g_lteAggregationThroughputScaleTestSuite;

LteAggregationThroughputScaleTestCase::LteAggregationThroughputScaleTestCase (std::string name)
  : TestCase (name)
{
  NS_LOG_FUNCTION (this << GetName ());
}


LteAggregationThroughputScaleTestCase::~LteAggregationThroughputScaleTestCase ()
{
  NS_LOG_FUNCTION (this << GetName ());
}

std::vector<ns3::Ptr<ns3::PacketSink>>
LteAggregationThroughputScaleTestCase::GetThroughput (uint8_t numberOfComponentCarriers, int noUe, double distance, uint8_t ulRb, uint8_t dlRb)
{
  NS_LOG_FUNCTION (this << GetName ());

  // EARFCN - E-UTRA Absolute Radio Frequency Channel Number

  Config::SetDefault ("ns3::LteEnbNetDevice::DlEarfcn", UintegerValue (100));
  Config::SetDefault ("ns3::LteEnbNetDevice::UlEarfcn", UintegerValue (100 + 18000));
  Config::SetDefault ("ns3::LteEnbNetDevice::DlBandwidth", UintegerValue (dlRb));
  Config::SetDefault ("ns3::LteEnbNetDevice::UlBandwidth", UintegerValue (ulRb));
  Config::SetDefault ("ns3::LteUeNetDevice::DlEarfcn", UintegerValue (100));

  auto lteHelper = CreateObject<LteHelper> ();
  //lteHelper->SetAttribute ("PathlossModel", TypeIdValue (ns3::FriisSpectrumPropagationLossModel::GetTypeId ()));
  // lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::NakagamiPropagationLossModel"));
  lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::LogDistancePropagationLossModel"));
  lteHelper->SetAttribute ("NumberOfComponentCarriers", UintegerValue (numberOfComponentCarriers));
  lteHelper->SetAttribute ("EnbComponentCarrierManager", StringValue ("ns3::RrComponentCarrierManager"));

  auto epcHelper = CreateObject<PointToPointEpcHelper> ();
  lteHelper->SetEpcHelper (epcHelper);

//  auto enbNode = CreateObject<Node> ();
//  auto ueNode = CreateObject<Node> ();
  NodeContainer enbNodes;
  NodeContainer ueNodes;
  enbNodes.Create(1);
  ueNodes.Create(noUe);
  
  auto pgwNode = epcHelper->GetPgwNode ();

  MobilityHelper mobility;
  mobility.SetPositionAllocator ("ns3::UniformDiscPositionAllocator",
                                 "X", DoubleValue (0.0),
                                 "Y", DoubleValue (0.0),
                                 "rho", DoubleValue (distance));

  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (enbNodes);
  mobility.Install (ueNodes);

  // For random positioning models, make sure AP is at (0, 0)
  Ptr<MobilityModel> mobilityEnb = enbNodes.Get(0)->GetObject<MobilityModel> ();
  Vector posEnb = mobilityEnb->GetPosition ();  
  posEnb.x = 0;
  posEnb.y = 0;
  mobilityEnb->SetPosition (posEnb);

  // Print position of each node
  std::cout << std::endl << "Node positions" << std::endl;

  //  - AP position
  Ptr<MobilityModel> positionEnb = enbNodes.Get(0)->GetObject<MobilityModel> ();
  posEnb = positionEnb->GetPosition ();
  std::cout << "AP position:\tx=" << posEnb.x << ", y=" << posEnb.y << std::endl;

  for (NodeContainer::Iterator j = ueNodes.Begin (); j != ueNodes.End (); ++j) 
    {
      Ptr<Node> object = *j;
      object = *j;
      Ptr<MobilityModel> position = object->GetObject<MobilityModel> ();
      position = object->GetObject<MobilityModel> ();
      Vector pos = position->GetPosition ();
      pos = position->GetPosition ();
      std::cout << "UE position: :\tx=" << pos.x << ", y=" << pos.y << std::endl;
    }

  InternetStackHelper internet;
  internet.Install (ueNodes);

  Ipv4AddressHelper ipv4h;
  ipv4h.SetBase ("1.0.0.0", "255.0.0.0");

  Ipv4StaticRoutingHelper ipv4RoutingHelper;
  auto ueStaticRouting = ipv4RoutingHelper.GetStaticRouting (ueNodes. Get(0)->GetObject<Ipv4> ());
  ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);

  auto enbDev = DynamicCast<LteEnbNetDevice> (lteHelper->InstallEnbDevice (enbNodes).Get (0));
  auto ueDevs = lteHelper->InstallUeDevice (ueNodes);
  auto ueDev = DynamicCast<LteUeNetDevice> (ueDevs.Get (0));

  Ipv4InterfaceContainer ueIpIface = epcHelper->AssignUeIpv4Address (ueDevs);

  for (int i = 0; i < noUe; i++)
  {
    // Attach to last CC as primary
    auto ueDev = DynamicCast<LteUeNetDevice> (ueDevs.Get (i));
    std::map< uint8_t, Ptr<ComponentCarrierUe> > ueCcMap =  ueDev->GetCcMap ();
    ueDev->SetDlEarfcn (ueCcMap.at (numberOfComponentCarriers - 1)->GetDlEarfcn ());
    lteHelper->Attach (ueDev);
    m_expectedCellId = enbDev->GetCcMap ().at (numberOfComponentCarriers - 1)->GetCellId ();
}

  // Applications
  const uint16_t port = 21;

  ApplicationContainer apps;

  std::vector<ns3::Ptr<ns3::PacketSink>> sinkVector;
  for (int i = 0; i < noUe; i++)
  {
    auto sink = CreateObject<PacketSink> ();
    sink->SetAttribute ("Protocol", StringValue ("ns3::UdpSocketFactory"));
    sink->SetAttribute ("Local", AddressValue (InetSocketAddress (ueIpIface.GetAddress (i), port)));
    ueNodes. Get(i)->AddApplication (sink);
    sinkVector.push_back(sink);
    apps.Add (sink);
  }

  std::vector<ns3::Ptr<ns3::UdpClient>> udpClientVector;
  for (int i = 0; i < noUe; i++)
  {
    auto client = CreateObject<UdpClient> ();
    client->SetAttribute ("RemotePort", UintegerValue (port));
    client->SetAttribute ("MaxPackets", UintegerValue (1000000));
    client->SetAttribute ("Interval", TimeValue (Seconds (0.0001)));
    client->SetAttribute ("PacketSize", UintegerValue (1000));
    client->SetAttribute ("RemoteAddress", AddressValue (ueIpIface.GetAddress (i)));
    udpClientVector.push_back(client);
    pgwNode->AddApplication (client);
    apps.Add (client);
  }

  apps.Start (Seconds (1.0));

  Simulator::Stop (Seconds (2.0));
  Simulator::Run ();

  m_actualCellId = ueDev->GetRrc ()->GetCellId ();

  Simulator::Destroy ();
  // std::cout << 8e-6 * sinkVector.at(1)->GetTotalRx () << std::endl; 
  // return 8e-6 * sinkVector.at(1)->GetTotalRx ();
  return sinkVector;
}

void LteAggregationThroughputScaleTestCase::RunSimulationSaveOutput(int noCarriers, int noUe, double distance, uint8_t ulRb, uint8_t dlRb)
{
  std::cout << "Simulation, number of carriers: " << noCarriers << " number of nodes: " << noUe << " distance: " << distance;
  std::vector<double> throughputs;
  std::vector<double> throughputsPerCarrier;

  std::vector<ns3::Ptr<ns3::PacketSink>> sinkVector = GetThroughput (noCarriers, noUe, distance, ulRb, dlRb);
  for (int i = 0; i < noUe; i++)
  {
    throughputs.push_back (8e-6 * sinkVector.at(i)->GetTotalRx ());
    std::cout << "UE " << i << " throughput: " << throughputs.at(i) << std::endl;
  }  

   double average = std::accumulate(begin (throughputsPerCarrier), end (throughputsPerCarrier), 0.0) / throughputsPerCarrier.size ();

    std::cout << "Aggregated throughput: " << std::accumulate(throughputs.begin(), throughputs.end(), 0.0) << std::endl << std::endl;

  for (double throughput: throughputsPerCarrier)
    {
      NS_TEST_ASSERT_MSG_EQ_TOL (throughput, average, average * 0.01, "Throughput does not scale with number of component carriers");
      // NS_TEST_ASSERT_MSG_EQ_TOL_INTERNAL (actual, limit, tol, msg, __FILE__, __LINE__)
    }
}


void
LteAggregationThroughputScaleTestCase::DoRun ()
{
  NS_LOG_ERROR ("Thank you for making a little script very happy ;D");
  RunSimulationSaveOutput(3,2, 100.0, 25, 25);
  RunSimulationSaveOutput(3,2, 500.0, 25, 25);
  RunSimulationSaveOutput(3,2, 1000.0, 25, 25);
  RunSimulationSaveOutput(3,20, 100.0, 25, 25);
  RunSimulationSaveOutput(5,2, 100.0, 25, 25);
}
